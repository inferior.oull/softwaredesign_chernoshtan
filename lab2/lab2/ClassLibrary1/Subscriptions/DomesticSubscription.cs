﻿namespace VideoProviderSubscriptions.Subscriptions
{
    public class DomesticSubscription : Subscription
    {
        public DomesticSubscription()
        {
            MonthlyFee = 5;
            MinPeriod = 1;
            Channels = new List<string> { "Channel 1", "Channel 2" };
            Features = new List<string> { "Feature A" };
        }

        public override string GetDetails()
        {
            return $"DomesticSubscription: {MonthlyFee}$ per month, min period: {MinPeriod} months, channels: {string.Join(", ", Channels)}, features: {string.Join(", ", Features)}";
        }
    }
}
