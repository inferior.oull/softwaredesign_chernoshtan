﻿namespace VideoProviderSubscriptions.Subscriptions
{
    public class EducationalSubscription : Subscription
    {
        public EducationalSubscription()
        {
            MonthlyFee = 2.50;
            MinPeriod = 6;
            Channels = new List<string> { "Channel 3", "Channel 4" };
            Features = new List<string> { "Feature B", "Feature C" };
        }

        public override string GetDetails()
        {
            return $"EducationalSubscription: {MonthlyFee}$ per month, min period: {MinPeriod} months, channels: {string.Join(", ", Channels)}, features: {string.Join(", ", Features)}";
        }
    }
}
