﻿namespace VideoProviderSubscriptions.Subscriptions
{
    public abstract class Subscription
    {
        public double MonthlyFee { get; protected set; }
        public int MinPeriod { get; protected set; }
        public List<string> Channels { get; protected set; }
        public List<string> Features { get; protected set; }

        public abstract string GetDetails();
    }
}
