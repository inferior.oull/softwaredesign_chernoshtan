﻿namespace VideoProviderSubscriptions.Subscriptions
{
    public class PremiumSubscription : Subscription
    {
        public PremiumSubscription()
        {
            MonthlyFee = 25;
            MinPeriod = 3;
            Channels = new List<string> { "Channel 5", "Channel 6", "Channel 7" };
            Features = new List<string> { "Feature D", "Feature E", "Feature F" };
        }

        public override string GetDetails()
        {
            return $"PremiumSubscription: {MonthlyFee}$ per month, min period: {MinPeriod} months, channels: {string.Join(", ", Channels)}, features: {string.Join(", ", Features)}";
        }
    }
}
