﻿using VideoProviderSubscriptions.Subscriptions;

namespace VideoProviderSubscriptions.Factories
{
    public abstract class SubscriptionFactory
    {
        public abstract Subscription CreateSubscription();
    }
}