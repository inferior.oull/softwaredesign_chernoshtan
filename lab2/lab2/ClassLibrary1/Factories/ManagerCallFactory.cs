﻿using VideoProviderSubscriptions.Subscriptions;

namespace VideoProviderSubscriptions.Factories
{
    public class ManagerCallFactory : SubscriptionFactory
    {
        public override Subscription CreateSubscription()
        {
            return new PremiumSubscription();
        }
    }
}
