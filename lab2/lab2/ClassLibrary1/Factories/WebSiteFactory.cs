﻿using VideoProviderSubscriptions.Subscriptions;

namespace VideoProviderSubscriptions.Factories
{
    public class WebSiteFactory : SubscriptionFactory
    {
        public override Subscription CreateSubscription()
        {
            return new DomesticSubscription();
        }
    }
}
