﻿using VideoProviderSubscriptions.Subscriptions;

namespace VideoProviderSubscriptions.Factories
{
    public class MobileAppFactory : SubscriptionFactory
    {
        public override Subscription CreateSubscription()
        {
            return new EducationalSubscription();
        }
    }
}
