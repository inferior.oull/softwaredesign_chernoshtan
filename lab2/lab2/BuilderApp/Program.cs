﻿using RpgClasses;

class Program
{
    static void Main(string[] args)
    {
        HeroBuilder heroBuilder = new HeroBuilder();
        CharacterDirector heroDirector = new CharacterDirector(heroBuilder);
        Character hero = heroDirector.ConstructCharacter(
            height: 185,
            build: "Athletic",
            hairColor: "Brunette",
            eyeColor: "Grey",
            clothing: "Scaled Armor",
            inventory: new string[] { "Rivers of Blood", "Uchigatana", "Flask of Crimson Tears" },
            goodDeeds: new string[] { "Maliketh, The Black Blade", "Defeated the Dragon of Darkwood" },
            evilDeeds: new string[] { "Burned the village of Eldoria", "Betrayed the Fellowship of Light" }
        );

        Console.WriteLine("Hero:");
        hero.DisplayCharacter();
        Console.WriteLine("\n");

        EnemyBuilder enemyBuilder = new EnemyBuilder();
        CharacterDirector enemyDirector = new CharacterDirector(enemyBuilder);
        Character enemy = enemyDirector.ConstructCharacter(
            height: 3000,
            build: "Muscular and big",
            hairColor: "Ginger",
            eyeColor: "Red",
            clothing: "Torn clothing",
            inventory: new string[] { "Fire bowl", "Flame of the Fallen God" },
            goodDeeds: new string[] { }, 
            evilDeeds: new string[] { "killed Queen Marika", "Burned the Erd tree" }
        );

        Console.WriteLine("Enemy:");
        enemy.DisplayCharacter();
    }
}