﻿namespace RpgClasses
{
    public class Character
    {
        public int Height { get; set; }
        public string Build { get; set; }
        public string HairColor { get; set; }
        public string EyeColor { get; set; }
        public string Clothing { get; set; }
        public string[] Inventory { get; set; }
        public string[] GoodDeeds { get; set; }
        public string[] EvilDeeds { get; set; }

        public void DisplayCharacter()
        {
            Console.WriteLine($"Height: {Height}, Build: {Build}, Hair Color: {HairColor}, Eye Color: {EyeColor}, Clothing: {Clothing}");

            Console.WriteLine("Inventory:");
            foreach (var item in Inventory)
            {
                Console.WriteLine($"- {item}");
            }

            Console.WriteLine("Good Deeds:");
            foreach (var deed in GoodDeeds)
            {
                Console.WriteLine($"- {deed}");
            }

            Console.WriteLine("Evil Deeds:");
            foreach (var deed in EvilDeeds)
            {
                Console.WriteLine($"- {deed}");
            }
        }
    }
}