﻿namespace RpgClasses
{
    public class CharacterDirector
    {
        private ICharacterBuilder characterBuilder;

        public CharacterDirector(ICharacterBuilder builder)
        {
            characterBuilder = builder;
        }

        public Character ConstructCharacter(int height, string build, string hairColor, string eyeColor, string clothing, string[] inventory, string[] goodDeeds, string[] evilDeeds)
        {
            return characterBuilder
                .SetHeight(height)
                .SetBuild(build)
                .SetHairColor(hairColor)
                .SetEyeColor(eyeColor)
                .SetClothing(clothing)
                .SetInventory(inventory)
                .AddGoodDeed(goodDeeds)
                .AddEvilDeed(evilDeeds)
                .Build();
        }
    }
}