﻿namespace RpgClasses
{
    public class EnemyBuilder : ICharacterBuilder
    {
        private Character character = new Character();

        public ICharacterBuilder SetHeight(int height)
        {
            character.Height = height;
            return this;
        }

        public ICharacterBuilder SetBuild(string build)
        {
            character.Build = build;
            return this;
        }

        public ICharacterBuilder SetHairColor(string hairColor)
        {
            character.HairColor = hairColor;
            return this;
        }

        public ICharacterBuilder SetEyeColor(string eyeColor)
        {
            character.EyeColor = eyeColor;
            return this;
        }

        public ICharacterBuilder SetClothing(string clothing)
        {
            character.Clothing = clothing;
            return this;
        }

        public ICharacterBuilder SetInventory(string[] inventory)
        {
            character.Inventory = inventory;
            return this;
        }

        public ICharacterBuilder AddGoodDeed(string[] goodDeeds)
        {
            character.GoodDeeds = goodDeeds;
            return this;
        }

        public ICharacterBuilder AddEvilDeed(string[] evilDeeds)
        {
            character.EvilDeeds = evilDeeds;
            return this;
        }

        public Character Build()
        {
            return character;
        }
    }
}