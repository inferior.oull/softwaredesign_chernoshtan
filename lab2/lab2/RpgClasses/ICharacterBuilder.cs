﻿namespace RpgClasses
{
    public interface ICharacterBuilder
    {
        ICharacterBuilder SetHeight(int height);
        ICharacterBuilder SetBuild(string build);
        ICharacterBuilder SetHairColor(string hairColor);
        ICharacterBuilder SetEyeColor(string eyeColor);
        ICharacterBuilder SetClothing(string clothing);
        ICharacterBuilder SetInventory(string[] inventory);
        ICharacterBuilder AddGoodDeed(string[] goodDeed);
        ICharacterBuilder AddEvilDeed(string[] evilDeed);
        Character Build();
    }
}