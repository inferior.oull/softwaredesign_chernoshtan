﻿public class Virus : ICloneable
{
    public int Weight { get; set; }
    public int Age { get; set; }
    public string Name { get; set; }
    public string Type { get; set; }
    public List<Virus> Children { get; set; }

    public Virus(int weight, int age, string name, string type)
    {
        Weight = weight;
        Age = age;
        Name = name;
        Type = type;
        Children = new List<Virus>();
    }

    public void AddChild(Virus child)
    {
        Children.Add(child);
    }

    public object Clone()
    {
        Virus clone = new Virus(this.Weight, this.Age, this.Name, this.Type);
        foreach (var child in this.Children)
        {
            clone.AddChild((Virus)child.Clone());
        }

        return clone;
    }

    public void PrintInfo()
    {
        Console.WriteLine($"Name: {Name}, Type: {Type}, Weight: {Weight}, Age: {Age}");
        foreach (var child in Children)
        {
            Console.Write("Child of ");
            child.PrintInfo();
        }
    }
}

class Program
{
    static void Main(string[] args)
    {
        Virus grandVirus = new Virus(123, 5, "Grandparent", "TypeA");
        Virus parentVirus = new Virus(103, 3, "Parent", "TypeB");
        Virus childVirus1 = new Virus(83, 2, "Child1", "TypeC");
        Virus childVirus2 = new Virus(63, 1, "Child2", "TypeD");

        grandVirus.AddChild(parentVirus);
        parentVirus.AddChild(childVirus1);
        parentVirus.AddChild(childVirus2);

        Virus clonedVirus = (Virus)grandVirus.Clone();

        Console.WriteLine("Virus:");
        grandVirus.PrintInfo();
        Console.WriteLine('\n');
        Console.WriteLine("Cloned virus:");
        clonedVirus.PrintInfo();
    }
}