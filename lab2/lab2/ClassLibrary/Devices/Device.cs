﻿namespace DeviceFactory.Devices
{
    public abstract class Device
    {
        public abstract string GetDetails();
    }
}
