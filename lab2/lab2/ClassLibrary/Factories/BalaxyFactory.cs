﻿namespace DeviceFactory.Factories
{
    using global::DeviceFactory.Brands.Balaxy;
    using global::DeviceFactory.Devices;

    public class BalaxyFactory : DeviceFactory
    {
        public override Laptop CreateLaptop()
        {
            return new BalaxyLaptop();
        }

        public override Netbook CreateNetbook()
        {
            return new BalaxyNetbook();
        }

        public override EBook CreateEBook()
        {
            return new BalaxyEBook();
        }

        public override Smartphone CreateSmartphone()
        {
            return new BalaxySmartphone();
        }
    }
}
