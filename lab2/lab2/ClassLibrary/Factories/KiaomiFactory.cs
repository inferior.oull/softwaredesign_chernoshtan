﻿namespace DeviceFactory.Factories
{
    using global::DeviceFactory.Brands.Kiaomi;
    using global::DeviceFactory.Devices;

    public class KiaomiFactory : DeviceFactory
    {
        public override Laptop CreateLaptop()
        {
            return new KiaomiLaptop();
        }

        public override Netbook CreateNetbook()
        {
            return new KiaomiNetbook();
        }

        public override EBook CreateEBook()
        {
            return new KiaomiEBook();
        }

        public override Smartphone CreateSmartphone()
        {
            return new KiaomiSmartphone();
        }
    }
}
