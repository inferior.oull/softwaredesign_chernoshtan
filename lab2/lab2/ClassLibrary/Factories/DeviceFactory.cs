﻿namespace DeviceFactory.Factories
{
    using global::DeviceFactory.Devices;

    public abstract class DeviceFactory
    {
        public abstract Laptop CreateLaptop();
        public abstract Netbook CreateNetbook();
        public abstract EBook CreateEBook();
        public abstract Smartphone CreateSmartphone();
    }
}
