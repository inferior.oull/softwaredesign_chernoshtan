﻿namespace DeviceFactory.Brands.Kiaomi
{
    using DeviceFactory.Devices;

    public class KiaomiNetbook : Netbook
    {
        public override string GetDetails()
        {
            return "Kiaomi Netbook";
        }
    }
}
