﻿namespace DeviceFactory.Brands.Kiaomi
{
    using DeviceFactory.Devices;

    public class KiaomiEBook : EBook
    {
        public override string GetDetails()
        {
            return "Kiaomi EBook";
        }
    }
}
