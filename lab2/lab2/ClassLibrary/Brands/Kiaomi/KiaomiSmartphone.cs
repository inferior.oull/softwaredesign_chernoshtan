﻿namespace DeviceFactory.Brands.Kiaomi
{
    using DeviceFactory.Devices;

    public class KiaomiSmartphone : Smartphone
    {
        public override string GetDetails()
        {
            return "Kiaomi Smartphone";
        }
    }
}
