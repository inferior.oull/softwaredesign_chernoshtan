﻿namespace DeviceFactory.Brands.Kiaomi
{
    using DeviceFactory.Devices;

    public class KiaomiLaptop : Laptop
    {
        public override string GetDetails()
        {
            return "Kiaomi Laptop";
        }
    }
}
