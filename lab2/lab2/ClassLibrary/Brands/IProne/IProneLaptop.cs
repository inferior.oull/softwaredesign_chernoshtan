﻿namespace DeviceFactory.Brands.Iprone
{
    using DeviceFactory.Devices;

    public class IProneLaptop : Laptop
    {
        public override string GetDetails()
        {
            return "IProne Laptop";
        }
    }
}
