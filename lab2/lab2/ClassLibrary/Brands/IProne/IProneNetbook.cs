﻿namespace DeviceFactory.Brands.Iprone
{
    using DeviceFactory.Devices;

    public class IProneNetbook : Netbook
    {
        public override string GetDetails()
        {
            return "IProne Netbook";
        }
    }
}
