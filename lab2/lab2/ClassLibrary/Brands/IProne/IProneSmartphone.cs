﻿namespace DeviceFactory.Brands.Iprone
{
    using DeviceFactory.Devices;

    public class IProneSmartphone : Smartphone
    {
        public override string GetDetails()
        {
            return "IProne Smartphone";
        }
    }
}
