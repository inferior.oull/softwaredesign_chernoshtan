﻿namespace DeviceFactory.Brands.Balaxy
{
    using DeviceFactory.Devices;

    public class BalaxyEBook : EBook
    {
        public override string GetDetails()
        {
            return "Balaxy EBook";
        }
    }
}
