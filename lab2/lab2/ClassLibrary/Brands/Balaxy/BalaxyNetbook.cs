﻿namespace DeviceFactory.Brands.Balaxy
{
    using DeviceFactory.Devices;

    public class BalaxyNetbook : Netbook
    {
        public override string GetDetails()
        {
            return "Balaxy Netbook";
        }
    }
}
