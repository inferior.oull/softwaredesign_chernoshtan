﻿namespace DeviceFactory.Brands.Balaxy
{
    using DeviceFactory.Devices;

    public class BalaxyLaptop : Laptop
    {
        public override string GetDetails()
        {
            return "Balaxy Laptop";
        }
    }
}
