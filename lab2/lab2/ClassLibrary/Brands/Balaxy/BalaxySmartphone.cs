﻿namespace DeviceFactory.Brands.Balaxy
{
    using DeviceFactory.Devices;

    public class BalaxySmartphone : Smartphone
    {
        public override string GetDetails()
        {
            return "Balaxy Smartphone";
        }
    }
}
