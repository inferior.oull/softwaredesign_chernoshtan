﻿using System;

namespace AbstractFactoryExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Authenticator authenticator1 = Authenticator.GetInstance();

            Authenticator authenticator2 = Authenticator.GetInstance();

            Console.WriteLine("Is authenticator1 the same instance as authenticator2? " + (authenticator1 == authenticator2));
        }
    }
}