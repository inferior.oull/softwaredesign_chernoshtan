﻿using DeviceFactory.Factories;

namespace AbstractFactoryTask2
{
    class Program
    {
        static void Main(string[] args)
        {
            DeviceFactory.Factories.DeviceFactory iproneFactory = new IProneFactory();
            TestFactory(iproneFactory);

            DeviceFactory.Factories.DeviceFactory kiaomiFactory = new KiaomiFactory();
            TestFactory(kiaomiFactory);

            DeviceFactory.Factories.DeviceFactory balaxyFactory = new BalaxyFactory();
            TestFactory(balaxyFactory);
        }

        static void TestFactory(DeviceFactory.Factories.DeviceFactory factory)
        {
            var laptop = factory.CreateLaptop();
            var netbook = factory.CreateNetbook();
            var eBook = factory.CreateEBook();
            var smartphone = factory.CreateSmartphone();

            Console.WriteLine(laptop.GetDetails());
            Console.WriteLine(netbook.GetDetails());
            Console.WriteLine(eBook.GetDetails());
            Console.WriteLine(smartphone.GetDetails());
        }
    }
}