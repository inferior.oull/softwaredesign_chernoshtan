﻿using System;

public class Authenticator
{
    private static Authenticator instance;

    private Authenticator()
    {

    }

    public static Authenticator GetInstance()
    {
        if (instance == null)
        {
            instance = new Authenticator();
        }

        return instance;
    }
}