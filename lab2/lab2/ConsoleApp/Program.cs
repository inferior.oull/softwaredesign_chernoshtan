﻿using VideoProviderSubscriptions.Subscriptions;
using VideoProviderSubscriptions.Factories;

namespace VideoProviderApp
{
    class Program
    {
        static void Main(string[] args)
        {
            SubscriptionFactory webSiteFactory = new WebSiteFactory();
            Subscription webSubscription = webSiteFactory.CreateSubscription();
            Console.WriteLine(webSubscription.GetDetails());
            Console.WriteLine("\n");
            SubscriptionFactory mobileAppFactory = new MobileAppFactory();
            Subscription mobileSubscription = mobileAppFactory.CreateSubscription();
            Console.WriteLine(mobileSubscription.GetDetails());
            Console.WriteLine("\n");
            SubscriptionFactory managerCallFactory = new ManagerCallFactory();
            Subscription managerSubscription = managerCallFactory.CreateSubscription();
            Console.WriteLine(managerSubscription.GetDetails());
        }
    }
}