﻿namespace AbstractFactoryTask3
{
    public class Authenticator
    {
        private static Authenticator instance;

        private Authenticator()
        {

        }

        public static Authenticator GetInstance()
        {
            if (instance == null)
            {
                instance = new Authenticator();
            }

            return instance;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Authenticator authenticator1 = Authenticator.GetInstance();

            Authenticator authenticator2 = Authenticator.GetInstance();

            Console.WriteLine("Is authenticators the same instances?\n" + (authenticator1 == authenticator2));
        }
    }
}