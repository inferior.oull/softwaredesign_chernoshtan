# Principles of programming. DRY, KISS, SOLID, YAGNI etc.

__In doing this lab, I had to use some of these programming principles:__
```
a. DRY,
b. KISS,
c. SOLID (5 окремих принципів)
d. YAGNI
e. Composition Over Inheritance
f. Program to Interfaces not Implementations
g. Fail Fas
```
__Used principles:__

1. __Fail Fast.__
[Example.](https://gitlab.com/inferior.oull/softwaredesign_chernoshtan/-/blob/main/lab1/lab1/ClassLibrary/Money.cs#L22-L35) This method will ensure the speedy detection and processing of errors.
2. __DRY.__ Most of the code is written so that all repeating checks and other lines of code are placed in separate methods. [Example.](https://gitlab.com/inferior.oull/softwaredesign_chernoshtan/-/blob/main/lab1/lab1/ClassLibrary/Warehouse.cs#L92-L102)
3. __Single Responsibility, SRP.__ Each class has only one reason for change. For example, [class Reporting](https://gitlab.com/inferior.oull/softwaredesign_chernoshtan/-/blob/main/lab1/lab1/ClassLibrary/Reporting.cs). It creates reports on the movement of goods in the warehouse. There is only one reason for changes: if you need to change the reporting logic or add new types of reports.
4. __Interface Segregation Principle, ISP.__ Clients should not depend on interfaces they do not use. For example, [ICurrency Interface](https://gitlab.com/inferior.oull/softwaredesign_chernoshtan/-/blob/main/lab1/lab1/ClassLibrary/Money.cs#L5-L9). This interface contains two properties: Name and Symbol. My code has no explicit dependency on this interface, so the ISP is respected.
5. __Dependency Inversion Principle, DIP.__ Top-level modules should not depend on lower-level modules. Both should depend on abstractions rather than concrete implementations. For example, [class Reporting](https://gitlab.com/inferior.oull/softwaredesign_chernoshtan/-/blob/main/lab1/lab1/ClassLibrary/Reporting.cs). Class accepts a dependency in its constructor - an object of type Warehouse, which is an abstraction (interface or base class). Class Reporting does not depend on the specific implementation of the warehouse, but only on its abstraction, which corresponds to the DIP principle.
6. __Composition over Inheritance.__ Give priority to composition, not inheritance. For example, [class Reporting](https://gitlab.com/inferior.oull/softwaredesign_chernoshtan/-/blob/main/lab1/lab1/ClassLibrary/Reporting.cs). Class Reporting contains a Warehouse object as part of its composition, rather than extending it through inheritance.
7. __KISS: Keep it simple, stupid__. The code should be simple and easy to understand. 
The methods in my classes have clear names and do one specific job, making them easy to understand and maintain. In addition, the code does not contain redundant or complex structures, which also complies with the KISS principle.