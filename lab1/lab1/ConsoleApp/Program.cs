﻿using ClassLibrary;
//1
Console.WriteLine("class Money");
Money money = new Money(10, 50);

money.DisplayAmount();
money.SetAmount();
money.DisplayAmount();

//2
Console.WriteLine("\nclass Product");
Product productApple = new Product("Apple", 5.23, 1);

productApple.DisplayProduct();
productApple.ReducePrice(6);
productApple.ReducePrice(3);
productApple.DisplayProduct();

//3
Console.WriteLine("\nclass Warehouse");
Warehouse warehouse = new Warehouse("Fruits and berries");
List<Product> products = new List<Product>();
products.Add(new Product("Grape", 7.47, 1));
products.Add(new Product("Pineapple", 10.78, 1));
products.Add(new Product("Strawberry", 15, 1));

foreach (Product product in products)
{
    warehouse.AddProduct(product, 25);
}
warehouse.DisplayProducts();
warehouse.RemoveProduct("Grape", 25);
warehouse.DisplayProducts();
warehouse.RemoveProduct("Pineapple", 10);
warehouse.DisplayProducts();
warehouse.ReduceProductPrice("Strawberry", 10);
warehouse.DisplayProduct("Strawberry");
warehouse.DisplayWarehousePrice();

//4
Console.WriteLine("\nclass Reporting");
Reporting reporting = new Reporting(warehouse);
Product productBanana = new Product("Banana", 7, 1);
Product productGrape = new Product("Grape", 10, 1);

reporting.RegisterIncoming(productBanana, 75);
reporting.RegisterIncoming(warehouse.FindProduct("Pineapple"), 15);
reporting.RegisterIncoming(productGrape, 15);
reporting.RegisterOutgoing(warehouse.FindProduct("Banana"), 25);
reporting.RegisterOutgoing(warehouse.FindProduct("Grape"), 5);
reporting.RegisterOutgoing(warehouse.FindProduct("Pineapple"), 10);
reporting.GenerateInventoryReport();

warehouse.DisplayProducts();