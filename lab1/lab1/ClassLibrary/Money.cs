﻿using System;

namespace ClassLibrary
{
    interface ICurrency
    {
        string Name { get; }
        string Symbol { get; }
    }

    public class Money
    {
        private int dollars;
        private int cents;

        public Money(int dollars, int cents)
        {
            this.dollars = dollars;
            this.cents = cents;
        }

        public int GetValidInt(string message)
        {
            int value;
            while (true)
            {
                Console.WriteLine(message);
                if (int.TryParse(Console.ReadLine(), out value))
                {
                    break;
                }
                Console.WriteLine("Invalid input! Please enter a valid integer.");
            }
            return value;
        }

        public void SetAmount()
        {
            this.dollars = GetValidInt("Enter amount of dollars: ");
            this.cents = GetValidInt("Enter amount of cents: ");
        }

        public void DisplayAmount()
        {
            Console.WriteLine($"Amount: {dollars}.{cents:00}");
        }
    }
}