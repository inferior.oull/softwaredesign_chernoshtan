﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ClassLibrary
{
    public class Warehouse
    {
        public string name;
        private List<Product> products;

        public List<Product> Products
        {
            get { return products; }
            set { products = value; }
        }

        public Warehouse(string name)
        {
            this.name = name;
            products = new List<Product>();
        }

        public void AddProduct(Product product, int quantity)
        {
            Product check = FindProduct(product.Name);
            if (check == null)
            {
                products.Add(product);
                product.Quantity = quantity;
            }
            else
            {
                product.Quantity += quantity;
            }
            Console.WriteLine($"Product '{product.Name}' added to warehouse '{name}' in quantity {quantity}.");
        }

        public void RemoveProduct(string productName, int quantity)
        {
            Product product = FindProduct(productName);
            if (product.Quantity == quantity)
            {
                products.Remove(product);
                Console.WriteLine($"Product '{product.Name}' removed from warehouse '{name}'");
                return;
            }
            product.Quantity -= quantity;
            Console.WriteLine($"Product '{product.Name}' removed from warehouse '{name}' in quantity {quantity}.");
        }

        public void DisplayProducts()
        {
            Console.WriteLine($"Products in warehouse '{name}':");
            foreach (var product in products)
            {
                Console.WriteLine($"- {product.Name}, Price: {product.Price}, Quantity: {product.Quantity}");
            }
        }

        public void DisplayProduct(string productName)
        {
            Product product = FindProduct(productName);
            if (product != null)
            {
                Console.WriteLine($"- {product.Name}, Price: {product.Price}, Quantity: {product.Quantity}");
            }
        }

        public void ReduceProductPrice(string productName, double amount)
        {
            Product product = FindProduct(productName);
            if (product != null)
            {
                product.ReducePrice(amount);
            }
        }

        public void DisplayWarehousePrice()
        {
            double totalPrice = 0;
            foreach (var product in products)
            {
                totalPrice += product.Price;
            }
            Console.WriteLine($"Total price of products in warehouse '{name}': {totalPrice}");
        }

        public Product FindProduct(string productName)
        {
            Product product = products.Find(p => p.Name == productName);
            if (product == null)
            {
                Console.WriteLine($"Product '{productName}' not found in warehouse '{name}'.");
                return null;
            }
            return product;
        }
    }
}
