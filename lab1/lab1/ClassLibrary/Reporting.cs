﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Reporting
    {
        private Warehouse warehouse;
        private List<Transaction> transactions;

        public Reporting(Warehouse warehouse)
        {
            this.warehouse = warehouse;
            transactions = new List<Transaction>();
        }

        public void RegisterIncoming(Product product, int quantity)
        {
            Transaction transaction = new Transaction(TransactionType.Incoming, product, quantity);
            transactions.Add(transaction);
            Console.WriteLine($"Incoming transaction registered: {quantity} {product.Name}(s)");
            warehouse.AddProduct(product, quantity);
        }

        public void RegisterOutgoing(Product product, int quantity)
        {
            Transaction transaction = new Transaction(TransactionType.Outgoing, product, quantity);
            transactions.Add(transaction);
            Console.WriteLine($"Outgoing transaction registered: {quantity} {product.Name}(s)");
            warehouse.RemoveProduct(product.Name, quantity);
        }

        public void GenerateInventoryReport()
        {
            Console.WriteLine($"Inventory report for warehouse '{warehouse.name}':");
            foreach (var product in warehouse.Products)
            {
                int totalQuantity = 0;
                foreach (var transaction in transactions)
                {
                    if (transaction.Product.Name == product.Name)
                    {
                        if (transaction.TransactionType == TransactionType.Incoming)
                        {
                            totalQuantity += transaction.Quantity;
                        }
                        else if (transaction.TransactionType == TransactionType.Outgoing)
                        {
                            totalQuantity -= transaction.Quantity;
                        }
                    }
                }
                Console.WriteLine($"- {product.Name}: {totalQuantity}");
            }
        }
    }

    public enum TransactionType
    {
        Incoming,
        Outgoing
    }

    public class Transaction
    {
        public TransactionType TransactionType { get; private set; }
        public Product Product { get; private set; }
        public int Quantity { get; private set; }

        public Transaction(TransactionType transactionType, Product product, int quantity)
        {
            TransactionType = transactionType;
            Product = product;
            Quantity = quantity;
        }
    }
}
