﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Product
    {
        public string Name;
        public double Price;
        public int Quantity;

        public Product(string name, double price, int quantity)
        {
            this.Name = name;
            this.Price = price;
            this.Quantity = quantity;
        }

        public void ReducePrice(double amount)
        {
            if (amount < 0)
            {
                Console.WriteLine("Invalid amount! Cannot reduce price by a negative value.");
                return;
            }

            if (amount > Price)
            {
                Console.WriteLine("Invalid amount! Cannot reduce price by more than the current price.");
                return;
            }

            Price = Math.Round(Price - amount, 2);
            Console.WriteLine($"Price of {Name} reduced by {amount}. New price: {Price}");
        }

        public void DisplayProduct()
        {
            Console.WriteLine($"Name: {Name}\nPrice: {Price}\nQuantity: {Quantity}");
        }
    }
}