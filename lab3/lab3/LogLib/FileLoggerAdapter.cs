﻿namespace LogLib
{
    public class FileLoggerAdapter : Logger
    {
        private readonly FileWriter fileWriter;

        public FileLoggerAdapter(string path)
        {
            fileWriter = new FileWriter(path);
        }

        public new void Log(string message)
        {
            fileWriter.WriteLine("[INFO] " + message);
        }

        public new void Error(string message)
        {
            fileWriter.WriteLine("[ERROR] " + message);
        }

        public new void Warn(string message)
        {
            fileWriter.WriteLine("[WARN] " + message);
        }
    }
}
