﻿namespace RpgClasses.RPGHeroes
{
    public class Warrior : Hero
    {
        public override string GetDescription()
        {
            return "Warrior";
        }

        public override int GetPower()
        {
            return 20;
        }
    }
}
