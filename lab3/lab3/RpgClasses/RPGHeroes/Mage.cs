﻿namespace RpgClasses.RPGHeroes
{
    public class Mage : Hero
    {
        public override string GetDescription()
        {
            return "Mage";
        }

        public override int GetPower()
        {
            return 50;
        }
    }
}
