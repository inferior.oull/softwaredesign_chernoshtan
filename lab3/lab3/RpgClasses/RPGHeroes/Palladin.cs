﻿namespace RpgClasses.RPGHeroes
{
    public class Palladin : Hero
    {
        public override string GetDescription()
        {
            return "Palladin";
        }

        public override int GetPower()
        {
            return 100;
        }
    }
}
