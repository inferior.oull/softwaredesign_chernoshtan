﻿namespace RpgClasses.RPGHeroes
{
    public abstract class Hero
    {
        public abstract string GetDescription();
        public abstract int GetPower();
    }
}
