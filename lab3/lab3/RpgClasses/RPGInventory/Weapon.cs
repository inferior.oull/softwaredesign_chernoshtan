﻿using RpgClasses.RPGHeroes;


namespace RpgClasses.RPGInventory
{
    public class Weapon : InventoryDecorator
    {
        public Weapon(Hero hero) : base(hero) { }

        public override string GetDescription()
        {
            return hero.GetDescription() + ", Weapon";
        }

        public override int GetPower()
        {
            return hero.GetPower() + 10;
        }
    }
}
