﻿using RpgClasses.RPGHeroes;


namespace RpgClasses.RPGInventory
{
    public class Armor : InventoryDecorator
    {
        public Armor(Hero hero) : base(hero) { }

        public override string GetDescription()
        {
            return hero.GetDescription() + ", Armor";
        }

        public override int GetPower()
        {
            return hero.GetPower() + 7;
        }
    }
}
