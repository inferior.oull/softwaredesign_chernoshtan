﻿using RpgClasses.RPGHeroes;


namespace RpgClasses.RPGInventory
{
    public class Artifact : InventoryDecorator
    {
        public Artifact(Hero hero) : base(hero) { }

        public override string GetDescription()
        {
            return hero.GetDescription() + ", Artifact";
        }

        public override int GetPower()
        {
            return hero.GetPower() + 23;
        }
    }
}
