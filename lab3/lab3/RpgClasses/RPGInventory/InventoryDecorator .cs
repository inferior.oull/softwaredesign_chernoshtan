﻿using RpgClasses.RPGHeroes;


namespace RpgClasses.RPGInventory
{
    public abstract class InventoryDecorator : Hero
    {
        protected Hero hero;

        public InventoryDecorator(Hero hero)
        {
            this.hero = hero;
        }

        public override string GetDescription()
        {
            return hero.GetDescription();
        }

        public override int GetPower()
        {
            return hero.GetPower();
        }
    }
}
