﻿using LightHTMLLib;

class Program
{
    static void Main(string[] args)
    {
        string filePath = "book.txt";
        var rootNode = LightHTMLParser.Parse(filePath);
        Console.WriteLine("HTML Structure:");
        Console.WriteLine(rootNode.OuterHTML);

        Console.WriteLine($"Size of tree in memory: {EstimateMemory(rootNode)} байт");
        OptimizeMemoryUsage(rootNode);

        Console.WriteLine("Optimized HTML Structure:");
        Console.WriteLine(rootNode.OuterHTML);
    }

    static long EstimateMemory(LightNode node)
    {
        long size = 0;
        if (node is LightElementNode elementNode)
        {
            size += sizeof(char) * elementNode.TagName.Length;
            size += sizeof(bool) * 2;
            foreach (var cssClass in elementNode.CssClasses)
            {
                size += sizeof(char) * cssClass.Length;
            }
            foreach (var child in elementNode.Children)
            {
                size += EstimateMemory(child);
            }
        }
        else if (node is LightTextNode textNode)
        {
            size += sizeof(char) * textNode.Text.Length;
        }

        return size;
    }

    static void OptimizeMemoryUsage(LightNode node)
    {
        if (node is LightElementNode elementNode)
        {
            elementNode.Children.RemoveAll(child => child is LightTextNode && string.IsNullOrWhiteSpace((child as LightTextNode).Text));
            foreach (var child in elementNode.Children)
            {
                OptimizeMemoryUsage(child);
            }
        }
    }
}