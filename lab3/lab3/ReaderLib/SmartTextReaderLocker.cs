﻿using System.Text.RegularExpressions;


namespace ReaderLib
{
    public class SmartTextReaderLocker : ISmartTextReader
    {
        private readonly ISmartTextReader _smartTextReader;
        private readonly Regex _regex;

        public SmartTextReaderLocker(ISmartTextReader smartTextReader, string pattern)
        {
            _smartTextReader = smartTextReader;
            _regex = new Regex(pattern, RegexOptions.Compiled);
        }

        public char[][] ReadFile(string path)
        {
            if (_regex.IsMatch(path))
            {
                Console.WriteLine("Access denied!");
                return null;
            }

            return _smartTextReader.ReadFile(path);
        }
    }
}
