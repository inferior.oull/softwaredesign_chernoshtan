﻿namespace ReaderLib
{
    public class SmartTextChecker : ISmartTextReader
    {
        private readonly ISmartTextReader _smartTextReader;

        public SmartTextChecker(ISmartTextReader smartTextReader)
        {
            _smartTextReader = smartTextReader;
        }

        public char[][] ReadFile(string path)
        {
            Console.WriteLine("Opening file.....");
            char[][] result = _smartTextReader.ReadFile(path);
            Console.WriteLine("File read successfully.");

            int lineCount = result.Length;
            int charCount = 0;
            foreach (var line in result)
            {
                charCount += line.Length;
            }

            Console.WriteLine($"Total lines: {lineCount}, Total characters: {charCount}");
            Console.WriteLine("Closing file.....");

            return result;
        }
    }
}
