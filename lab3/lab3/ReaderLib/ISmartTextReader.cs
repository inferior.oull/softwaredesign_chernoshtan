﻿namespace ReaderLib
{
    public interface ISmartTextReader
    {
        char[][] ReadFile(string path);
    }
}