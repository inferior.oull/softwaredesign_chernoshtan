﻿using LightHTMLLib;


class Program
{
    static void Main(string[] args)
    {
        var table = new LightElementNode("table", true, false);
        table.CssClasses.Add("table");
        table.CssClasses.Add("table-bordered");

        var thead = new LightElementNode("thead", true, false);
        var tbody = new LightElementNode("tbody", true, false);

        var headerRow = new LightElementNode("tr", true, false);
        var header1 = new LightElementNode("th", false, false);
        header1.Children.Add(new LightTextNode("Rank"));
        var header2 = new LightElementNode("th", false, false);
        header2.Children.Add(new LightTextNode("Weapon"));
        var header3 = new LightElementNode("th", false, false);
        header3.Children.Add(new LightTextNode("Description"));

        headerRow.Children.Add(header1);
        headerRow.Children.Add(header2);
        headerRow.Children.Add(header3);

        thead.Children.Add(headerRow);

        var row1 = new LightElementNode("tr", true, false);
        var rank1 = new LightElementNode("td", false, false);
        rank1.Children.Add(new LightTextNode("1"));
        var weapon1 = new LightElementNode("td", false, false);
        weapon1.Children.Add(new LightTextNode("Dark Moon Greatsword"));
        var desc1 = new LightElementNode("td", false, false);
        desc1.Children.Add(new LightTextNode("A greatsword that scales with Intelligence and deals frost damage."));

        row1.Children.Add(rank1);
        row1.Children.Add(weapon1);
        row1.Children.Add(desc1);

        tbody.Children.Add(row1);

        var row2 = new LightElementNode("tr", true, false);
        var rank2 = new LightElementNode("td", false, false);
        rank2.Children.Add(new LightTextNode("2"));
        var weapon2 = new LightElementNode("td", false, false);
        weapon2.Children.Add(new LightTextNode("Blasphemous Blade"));
        var desc2 = new LightElementNode("td", false, false);
        desc2.Children.Add(new LightTextNode("A greatsword that steals HP from enemies on hit."));

        row2.Children.Add(rank2);
        row2.Children.Add(weapon2);
        row2.Children.Add(desc2);

        tbody.Children.Add(row2);

        var row3 = new LightElementNode("tr", true, false);
        var rank3 = new LightElementNode("td", false, false);
        rank3.Children.Add(new LightTextNode("3"));
        var weapon3 = new LightElementNode("td", false, false);
        weapon3.Children.Add(new LightTextNode("River of Blood"));
        var desc3 = new LightElementNode("td", false, false);
        desc3.Children.Add(new LightTextNode("A massive greatsword that causes bleeding on hit."));

        row3.Children.Add(rank3);
        row3.Children.Add(weapon3);
        row3.Children.Add(desc3);

        tbody.Children.Add(row3);

        table.Children.Add(thead);
        table.Children.Add(tbody);

        Console.WriteLine(table.OuterHTML);
    }
}