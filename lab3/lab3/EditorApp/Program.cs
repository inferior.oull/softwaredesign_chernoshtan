﻿using GraphicLib.GraphicRenderers;
using GraphicLib.GraphicShapes;

namespace GraphicEditor
{
    class Program
    {
        static void Main(string[] args)
        {
            IRenderer vectorRenderer = new VectorRenderer();
            IRenderer rasterRenderer = new RasterRenderer();

            Shape circle = new Circle(rasterRenderer);
            Shape square = new Square(vectorRenderer);
            Shape triangle = new Triangle(rasterRenderer);

            circle.Draw();
            square.Draw();
            triangle.Draw();
        }
    }
}