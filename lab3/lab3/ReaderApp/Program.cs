﻿using ReaderLib;

namespace TextReaderApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = "test.txt";
            string restrictedFilePath = "restricted_test.txt";

            ISmartTextReader reader = new SmartTextReader();
            ISmartTextReader checker = new SmartTextChecker(reader);
            ISmartTextReader locker = new SmartTextReaderLocker(checker, "restricted_.*\\.txt");

            Console.WriteLine("Trying to read normal file:");
            locker.ReadFile(filePath);

            Console.WriteLine("\nTrying to read restricted file:");
            locker.ReadFile(restrictedFilePath);
        }
    }
}