﻿using RpgClasses.RPGHeroes;
using RpgClasses.RPGInventory;


namespace RPGGame
{
    class Program
    {
        static void Main(string[] args)
        {
            Hero warrior = new Warrior();
            Console.WriteLine($"{warrior.GetDescription()}. Power: {warrior.GetPower()}");

            Hero mage = new Mage();
            Console.WriteLine($"{mage.GetDescription()}. Power: {mage.GetPower()}");

            Hero palladin = new Palladin();
            Console.WriteLine($"{palladin.GetDescription()}. Power: {palladin.GetPower()}");

            warrior = new Armor(warrior);
            Console.WriteLine($"{warrior.GetDescription()}. Power: {warrior.GetPower()}");

            warrior = new Weapon(warrior);
            Console.WriteLine($"{warrior.GetDescription()}. Power: {warrior.GetPower()}");

            warrior = new Artifact(warrior);
            Console.WriteLine($"{warrior.GetDescription()}. Power: {warrior.GetPower()}");

            mage = new Artifact(mage);
            Console.WriteLine($"{mage.GetDescription()}. Power: {mage.GetPower()}");

            palladin = new Armor(palladin);
            palladin = new Weapon(palladin);
            Console.WriteLine($"{palladin.GetDescription()}. Power: {palladin.GetPower()}");
        }
    }
}