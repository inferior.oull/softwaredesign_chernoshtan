﻿using GraphicLib.GraphicRenderers;


namespace GraphicLib.GraphicShapes
{
    public class Triangle : Shape
    {
        public Triangle(IRenderer renderer) : base(renderer) { }

        public override void Draw()
        {
            renderer.Render("Triangle");
        }
    }
}
