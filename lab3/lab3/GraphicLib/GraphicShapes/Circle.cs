﻿using GraphicLib.GraphicRenderers;


namespace GraphicLib.GraphicShapes
{
    public class Circle : Shape
    {
        public Circle(IRenderer renderer) : base(renderer) { }

        public override void Draw()
        {
            renderer.Render("Circle");
        }
    }
}
