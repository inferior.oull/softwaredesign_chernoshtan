﻿namespace GraphicLib.GraphicRenderers
{
    public class VectorRenderer : IRenderer
    {
        public void Render(string shape)
        {
            Console.WriteLine($"Drawing {shape} as lines.");
        }
    }
}
