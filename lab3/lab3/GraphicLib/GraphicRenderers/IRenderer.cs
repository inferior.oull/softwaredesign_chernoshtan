﻿namespace GraphicLib.GraphicRenderers
{
    public interface IRenderer
    {
        void Render(string shape);
    }
}
