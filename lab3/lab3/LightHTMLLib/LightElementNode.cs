﻿using System.Text;

namespace LightHTMLLib
{
    public class LightElementNode : LightNode
    {
        public string TagName { get; set; }
        public bool IsBlock { get; set; }
        public bool IsSelfClosing { get; set; }
        public List<string> CssClasses { get; set; }
        public List<LightNode> Children { get; set; }

        public LightElementNode(string tagName, bool isBlock, bool isSelfClosing)
        {
            TagName = tagName;
            IsBlock = isBlock;
            IsSelfClosing = isSelfClosing;
            CssClasses = new List<string>();
            Children = new List<LightNode>();
        }

        public override string OuterHTML
        {
            get
            {
                var html = new StringBuilder();
                html.Append($"<{TagName}");
                if (CssClasses.Count > 0)
                {
                    html.Append($" class=\"{string.Join(" ", CssClasses)}\"");
                }
                if (IsSelfClosing)
                {
                    html.Append(" />");
                }
                else
                {
                    html.Append(">");
                    html.Append(InnerHTML);
                    html.Append($"</{TagName}>");
                }

                return html.ToString();
            }
        }

        public override string InnerHTML
        {
            get
            {
                var html = new StringBuilder();
                foreach (var child in Children)
                {
                    html.Append(child.OuterHTML);
                }
                return html.ToString();
            }
        }
    }
}