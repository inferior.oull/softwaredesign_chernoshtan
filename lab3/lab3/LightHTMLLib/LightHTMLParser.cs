﻿namespace LightHTMLLib
{
    public static class LightHTMLParser
    {
        public static LightElementNode Parse(string filePath)
        {
            var lines = File.ReadAllLines(filePath);
            LightElementNode rootNode = new LightElementNode("body", true, false);

            foreach (var line in lines)
            {
                if (line.Length < 20)
                {
                    var node = new LightElementNode("h2", true, false);
                    node.Children.Add(new LightTextNode(line));
                    rootNode.Children.Add(node);
                }
                else if (line.StartsWith(" "))
                {
                    var node = new LightElementNode("blockquote", true, false);
                    node.Children.Add(new LightTextNode(line));
                    rootNode.Children.Add(node);
                }
                else
                {
                    var node = new LightElementNode("p", true, false);
                    node.Children.Add(new LightTextNode(line));
                    rootNode.Children.Add(node);
                }
            }

            return rootNode;
        }
    }
}
